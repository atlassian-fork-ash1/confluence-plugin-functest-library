package com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.BandanaHelper;
import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import com.atlassian.confluence.plugin.functest.helper.GroupHelper;
import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.helper.IndexHelper;
import com.atlassian.confluence.plugin.functest.helper.MailServerHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import net.sourceforge.jwebunit.junit.WebTestCase;

import java.io.File;
import java.io.IOException;

@SuppressWarnings({ "UnusedDeclaration" })
public class AbstractConfluencePluginWebTestCase extends WebTestCase
{
    protected JWebUnitConfluenceWebTester createConfluenceWebTester()
    {
        try
        {
            return new JWebUnitConfluenceWebTester(new TesterConfiguration());
        }
        catch (IOException ioe)
        {
            fail("Unable to create tester: " + ioe.getMessage());
            return null;
        }
    }

    @SuppressWarnings({ "deprecation" })
    protected void setUp() throws Exception
    {
        super.setUp();
        tester = createConfluenceWebTester(); // Override JWebUnit's tester

        beginAt("/");
        loginAsAdmin();
        installTestLibrary();
        installPlugin();
        updateLicense();
        restoreData();
        updateBaseUrl();
    }

    private void updateBaseUrl()
    {
        try
        {
            getConfluenceWebTester().gotoPageWithEscalatedPrivileges("/admin/editgeneralconfig.action");
            setWorkingForm("editgeneralconfig");
            setTextField("domainName", getConfluenceWebTester().getBaseUrl());
            submit("confirm");
        }
        finally
        {
            getConfluenceWebTester().dropEscalatedPrivileges();
        }
    }

    protected void tearDown() throws Exception
    {
        logout();
        super.tearDown();
    }

    protected ConfluenceWebTester getConfluenceWebTester()
    {
        return (ConfluenceWebTester) tester;
    }

    public void dropEscalatedPrivileges()
    {
        getConfluenceWebTester().dropEscalatedPrivileges();
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination, String urlEncoding)
    {
        return getConfluenceWebTester().gotoPageWithEscalatedPrivileges(destination, urlEncoding);
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination)
    {
        return getConfluenceWebTester().gotoPageWithEscalatedPrivileges(destination);
    }

    protected void login(String username, String password)
    {
        getConfluenceWebTester().login(username, password);
    }

    protected void login()
    {
        getConfluenceWebTester().login();
    }

    protected void loginAsAdmin()
    {
        getConfluenceWebTester().login(getConfluenceWebTester().getAdminUserName(), getConfluenceWebTester().getAdminPassword());
    }

    protected void logout()
    {
        getConfluenceWebTester().logout();
    }

    public void updateLicense() throws IOException
    {
        getConfluenceWebTester().updateLicense();
    }

    public void refreshLicense()
    {
        getConfluenceWebTester().refreshLicense();
    }

    public void installTestLibrary()
    {
        getConfluenceWebTester().installTestLibrary();
    }

    public void installPlugin()
    {
        getConfluenceWebTester().installPlugin();
    }

    /**
     * Restore the class path resource &quot;site-export.zip&quot; into Confluence.
     * @deprecated
     * Please use {@link #restoreData(java.io.File)} instead.
     */
    @SuppressWarnings({ "deprecation" })
    public void restoreData()
    {
        getConfluenceWebTester().restoreData();
    }

    public void restoreData(File exportZip)
    {
        getConfluenceWebTester().restoreData(exportZip);
    }

    public void flushCaches()
    {
        getConfluenceWebTester().flushCaches();
    }

    /**
     * @deprecated Please use {@link com.atlassian.confluence.plugin.functest.helper.IndexHelper#update()} instead.
     */
    public void reindex()
    {
        getIndexHelper().update();
    }

    public void assertResourceXsrfProtected(String resourcePath)
    {
        getConfluenceWebTester().assertResourceXsrfProtected(resourcePath);
    }

    public PageHelper getPageHelper(long pageId)
    {
        return HelperFactory.createPageHelper(getConfluenceWebTester(), pageId);
    }

    public PageHelper getPageHelper()
    {
        return HelperFactory.createPageHelper(getConfluenceWebTester());
    }

    public BlogPostHelper getBlogPostHelper(long blogPostId)
    {
        return HelperFactory.createBlogPostHelper(getConfluenceWebTester(), blogPostId);
    }

    public BlogPostHelper getBlogPostHelper()
    {
        return HelperFactory.createBlogPostHelper(getConfluenceWebTester());
    }

    public SpaceHelper getSpaceHelper(String spaceKey)
    {
        return HelperFactory.createSpaceHelper(getConfluenceWebTester(), spaceKey);
    }

    public SpaceHelper getSpaceHelper()
    {
        return HelperFactory.createSpaceHelper(getConfluenceWebTester());
    }

    public CommentHelper getCommentHelper(long commentId)
    {
        return HelperFactory.createCommentHelper(getConfluenceWebTester(), commentId);
    }

    public CommentHelper getCommentHelper()
    {
        return HelperFactory.createCommentHelper(getConfluenceWebTester());
    }

    public UserHelper getUserHelper(String userName)
    {
        return HelperFactory.createUserHelper(getConfluenceWebTester(), userName);
    }

    public UserHelper getUserHelper()
    {
        return HelperFactory.createUserHelper(getConfluenceWebTester());
    }

    public GroupHelper getGroupHelper(String groupName)
    {
        return HelperFactory.createGrouphelper(getConfluenceWebTester(), groupName);
    }

    public GroupHelper getGroupHelper()
    {
        return HelperFactory.createGrouphelper(getConfluenceWebTester());
    }

    public MailServerHelper getMailServerHelper(long id)
    {
        return HelperFactory.createMailServerHelper(getConfluenceWebTester(), id);
    }

    public MailServerHelper getMailServerHelper()
    {
        return HelperFactory.createMailServerHelper(getConfluenceWebTester());
    }

    public AttachmentHelper getAttachmentHelper()
    {
        return HelperFactory.createAttachmentHelper(getConfluenceWebTester());
    }

    public AttachmentHelper getAttachmentHelper(long id, String fileName)
    {
        return HelperFactory.createAttachmentHelper(getConfluenceWebTester(), id, fileName);
    }

    public BandanaHelper getBandanaHelper(String spaceKey, String bandanaKey)
    {
        return HelperFactory.createBandanaHelper(getConfluenceWebTester(), spaceKey, bandanaKey);
    }

    public BandanaHelper getBandanaHelper(String bandanaKey)
    {
        return HelperFactory.createBandanaHelper(getConfluenceWebTester(), "", bandanaKey);
    }

    public BandanaHelper getBandanaHelper()
    {
        return HelperFactory.createBandanaHelper(getConfluenceWebTester(), "");
    }

    public IndexHelper getIndexHelper()
    {
        return HelperFactory.createIndexHelper(getConfluenceWebTester());
    }

}
