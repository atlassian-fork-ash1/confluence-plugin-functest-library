package com.atlassian.confluence.plugin.functest;

import junit.framework.TestCase;

import java.io.IOException;

public class ConfluencePluginSeleniumTestCaseBase extends TestCase
{
    protected SeleniumConfluenceWebTester tester;

    protected SeleniumConfluenceWebTester createConfluenceWebTester()
    {
        try
        {
            return new SeleniumConfluenceWebTester(new TesterConfiguration(), getSeleniumServerPort(), getBrowserStartCommand());
        }
        catch (IOException ioe)
        {
            fail("Unable to create tester: " + ioe.getMessage());
            return null;
        }
    }

    protected String getBrowserStartCommand()
    {
        return System.getProperty("com.atlassian.confluence.plugin.functest.selenium.browser");
    }

    protected int getSeleniumServerPort()
    {
        return Integer.getInteger("com.atlassian.confluence.plugin.functest.selenium.port", 4444);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        tester = createConfluenceWebTester();
        tester.start();
        
        tester.login();
        tester.installTestLibrary();
        tester.installPlugin();
        tester.updateLicense();
        tester.restoreData();

        // Update base URL
        tester.open(tester.getContextPath() + "/admin/editgeneralconfig.action");
        tester.type("css=form[name='editgeneralconfig'] input[name='domainName']", tester.getBaseUrl());
        tester.submit("css=form[name='editgeneralconfig']");
        tester.waitForPageToLoad();
    }

    protected void tearDown() throws Exception
    {
        tester.logout();
        tester.stop();
        super.tearDown();
    }

    protected ConfluenceWebTester getTester()
    {
        return tester;
    }
}
