package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

public abstract class ContentHelper extends AbstractHelper
{

    private static Logger LOG = Logger.getLogger(ContentHelper.class);

    private long id;

    private String content;

    private String creator;

    private Date creationDate;

    private int version; /* Current version */

    protected ContentHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester);
        setId(id);
    }

    protected ContentHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    protected boolean saveOrUpdate()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return storeRemoteContentAndUpdateHelper(soapSessionToken, confluenceSoapService);

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean create()
    {
        return saveOrUpdate();
    }

    public boolean read()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return readRemoteContentAndUpdateHelper(soapSessionToken, confluenceSoapService);

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean update()
    {
        return saveOrUpdate();
    }

    public boolean delete()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return deleteRemoteContent(soapSessionToken, confluenceSoapService);

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    protected abstract boolean storeRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException;

    protected abstract boolean readRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException;

    protected abstract boolean deleteRemoteContent(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException;


    public String getContentAsXhtml(String markup)
    {
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (String) confluenceWebTester.getXmlRpcClient().execute("functest-rendering.convertMarkupToXhtml",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    markup,
                                    String.valueOf(getId())
                            )
                    ));

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return null;
    }

    public String getContentAsXhtml()
    {
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (String) confluenceWebTester.getXmlRpcClient().execute("functest-rendering.getViewFormat",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    String.valueOf(getId())
                            )
                    ));

        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to render content %d to XHTML", getId()), e);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return null;
    }

    public String getXhtmlAsMarkup(String xhtml)
    {
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (String) confluenceWebTester.getXmlRpcClient().execute("functest-rendering.convertXhtmlToMarkup",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    xhtml,
                                    String.valueOf(getId())
                            )
                    ));

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return null;
    }
}
