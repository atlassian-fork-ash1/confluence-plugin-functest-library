package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteAttachment;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteBlogEntry;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Vector;

public class BlogPostHelper extends AbstractSpaceContentHelper implements Attachable
{

    private static final Logger LOG = Logger.getLogger(BlogPostHelper.class);

    public BlogPostHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester, id);
    }

    public BlogPostHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    protected RemoteBlogEntry toRemoteBlogEntry()
    {
        final RemoteBlogEntry remoteBlogEntry = new RemoteBlogEntry();
        final Calendar creationDate = Calendar.getInstance();

        remoteBlogEntry.setId(getId());
        remoteBlogEntry.setSpace(getSpaceKey());
        remoteBlogEntry.setTitle(getTitle());
        remoteBlogEntry.setVersion(getVersion());
        remoteBlogEntry.setContent(getContent());
        remoteBlogEntry.setAuthor(StringUtils.isBlank(getCreator()) ? confluenceWebTester.getCurrentUserName() : getCreator());

        creationDate.setTime(getCreationDate());
        remoteBlogEntry.setPublishDate(creationDate);

        return remoteBlogEntry;
    }

    protected void populateHelper(final Map<String, ?> blogStruct)
    {
        setId(Long.parseLong((String) blogStruct.get("id")));
        setSpaceKey((String) blogStruct.get("spaceKey"));
        setTitle((String) blogStruct.get("title"));
        setVersion((Integer) blogStruct.get("version"));
        setContent((String) blogStruct.get("content"));
        setCreator((String) blogStruct.get("creator"));
        setCreationDate((Date) blogStruct.get("created"));
    }

    protected boolean storeRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        final RemoteBlogEntry remoteBlogEntry = confluenceSoapService.storeBlogEntry(soapSessionToken, toRemoteBlogEntry());

        setId(remoteBlogEntry.getId());
        setVersion(remoteBlogEntry.getVersion());
        return 0 < getId();
    }

    protected boolean readRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        boolean success = false;
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();

            @SuppressWarnings("unchecked")
            Map<String, ?> blogStruct = (Map<String, ?>) confluenceWebTester.getXmlRpcClient().execute("functest-blogpost.getBlogPost",
                    new Vector<Object>(
                            Arrays.asList(
                                    authenticationToken,
                                    String.valueOf(getId())
                            )
                    ));
            if ((success = null != blogStruct))
                populateHelper(blogStruct);
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to find blog post with ID %d", getId()), e);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return success;
    }

    protected boolean deleteRemoteContent(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        return confluenceSoapService.removePage(soapSessionToken, getId());
    }

    public String[] getAttachmentFileNames()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteAttachment[] remoteAttachments;
            final String[] attachmentNames;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteAttachments = confluenceSoapService.getAttachments(soapSessionToken, getId());

            if (null != remoteAttachments)
            {
                attachmentNames = new String[remoteAttachments.length];

                for (int i = 0; i < remoteAttachments.length; ++i)
                {
                    attachmentNames[i] = remoteAttachments[i].getFileName();
                }

            }
            else
            {
                attachmentNames = new String[0];
            }

            return attachmentNames;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return null;
    }

    public long findBySpaceKeyPublishedDateAndBlogPostTitle()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient;
            final String blogPostId;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            blogPostId = (String) xmlRpcClient.execute("functest-blogpost.getBlogPostId",
                    new Vector<Object>(
                            Arrays.asList(
                                    authenticationToken,
                                    getSpaceKey(),
                                    getTitle(),
                                    getCreationDate()
                            )
                    ));

            return StringUtils.isBlank(blogPostId) ? 0 : Long.parseLong(blogPostId);

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return 0;
    }
}
