package com.atlassian.confluence.plugin.functest.helper;

import java.util.List;

public interface Labellable
{

    void setLabels(final List<String> labels);

    List<String> getLabels();
}
