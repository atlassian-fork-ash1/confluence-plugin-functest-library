package com.atlassian.confluence.plugin.functest.module.xmlrpc.blog;

import com.atlassian.confluence.rpc.RemoteException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Date;
import java.util.Map;

public class BlogPostHelperServiceImpl implements BlogPostHelperService
{

    private BlogPostHelperService blogPostHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    public void setBlogPostHelperServiceDelegate(BlogPostHelperService blogPostHelperServiceDelegate)
    {
        this.blogPostHelperServiceDelegate = blogPostHelperServiceDelegate;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public String getBlogPostId(final String authenticationToken, final String spaceKey, final String title, final Date day)
    {
        return (String) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return blogPostHelperServiceDelegate.getBlogPostId(authenticationToken, spaceKey, title, day);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    @Override
    public Map<String, ?> getBlogPost(final String authenticationToken, final String id)
    {
        return (Map<String, ?>) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return blogPostHelperServiceDelegate.getBlogPost(authenticationToken, id);
                    }
                }
        );
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
