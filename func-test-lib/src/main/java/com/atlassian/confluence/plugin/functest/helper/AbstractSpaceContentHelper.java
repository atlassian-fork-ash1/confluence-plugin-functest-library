package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteLabel;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public abstract class AbstractSpaceContentHelper extends ContentHelper
        implements Labellable, Commentable
{
    private static final Logger LOG = Logger.getLogger(AbstractSpaceContentHelper.class);

    private String title;

    private String spaceKey;

    private List<String> labels;

    protected AbstractSpaceContentHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester, id);
        setLabels(new ArrayList<String>());
    }

    protected AbstractSpaceContentHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public List<String> getLabels()
    {
        return labels;
    }

    public void setLabels(List<String> labels)
    {
        this.labels = labels;
    }

    /**
     * @return The content rendered (in HTML)
     * @deprecated
     */
    public String getContentRendered()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.renderContent(
                    soapSessionToken, getSpaceKey(), getId(), getContent());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return null;
    }

    public void markFavourite()
    {
        String xmlRpcAuthToken = null;

        try
        {
            XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            xmlRpcAuthToken = confluenceWebTester.loginToXmlRpcService(
                    confluenceWebTester.getCurrentUserName(),
                    confluenceWebTester.getCurrentPassword()
            );

            Boolean success = (Boolean) xmlRpcClient.execute(
                    "functest-content.markFavorite",
                    new Vector<String>(
                            asList(
                                    xmlRpcAuthToken,
                                    String.valueOf(getId())
                            )
                    )
            );

            if (!success)
            {
                throw new IllegalStateException("Unable to mark content#" + getId() + " as favorite. Does it exist as a non-favored content?");
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(xmlRpcAuthToken);
        }
    }

    public void addWatch()
    {
        String xmlRpcAuthToken = null;

        try
        {
            XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            xmlRpcAuthToken = confluenceWebTester.loginToXmlRpcService(
                    confluenceWebTester.getCurrentUserName(),
                    confluenceWebTester.getCurrentPassword()
            );

            Boolean success = (Boolean) xmlRpcClient.execute(
                    "functest-content.addWatch",
                    new Vector<String>(
                            asList(
                                    xmlRpcAuthToken,
                                    String.valueOf(getId())
                            )
                    )
            );

            if (!success)
            {
                throw new IllegalStateException("Unable to watch content#" + getId() + ". Does it exist as a a content that is not watched by " + confluenceWebTester.getCurrentUserName());
            }
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(xmlRpcAuthToken);
        }
    }

    public void removeWatch()
    {
        String xmlRpcAuthToken = null;

        try
        {
            XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            xmlRpcAuthToken = confluenceWebTester.loginToXmlRpcService(
                    confluenceWebTester.getCurrentUserName(),
                    confluenceWebTester.getCurrentPassword()
            );

            Boolean success = (Boolean) xmlRpcClient.execute(
                    "functest-content.removeWatch",
                    new Vector<String>(
                            asList(
                                    xmlRpcAuthToken,
                                    String.valueOf(getId())
                            )
                    )
            );

            if (!success)
            {
                throw new IllegalStateException("Unable to remove watch on content#" + getId() + ". Does it exist as a content that is watched by " + confluenceWebTester.getCurrentUserName());
            }
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(xmlRpcAuthToken);
        }
    }

    public boolean isWatched()
    {
        String xmlRpcAuthToken = null;

        try
        {
            XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            xmlRpcAuthToken = confluenceWebTester.loginToXmlRpcService(
                    confluenceWebTester.getCurrentUserName(),
                    confluenceWebTester.getCurrentPassword()
            );

            return (Boolean) xmlRpcClient.execute(
                    "functest-content.isWatched",
                    new Vector<String>(
                            asList(
                                    xmlRpcAuthToken,
                                    String.valueOf(getId())
                            )
                    )
            );
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(xmlRpcAuthToken);
        }

        return false;
    }

    public boolean isFavorite()
    {
        String xmlRpcAuthToken = null;

        try
        {
            XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            xmlRpcAuthToken = confluenceWebTester.loginToXmlRpcService(
                    confluenceWebTester.getCurrentUserName(),
                    confluenceWebTester.getCurrentPassword()
            );

            return (Boolean) xmlRpcClient.execute(
                    "functest-content.isFavorite",
                    new Vector<String>(
                            asList(
                                    xmlRpcAuthToken,
                                    String.valueOf(getId())
                            )
                    )
            );
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException xmlRpcE)
        {
            LOG.error("Service request denied.", xmlRpcE);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(xmlRpcAuthToken);
        }

        return false;
    }

    public List<Long> getCommentIds()
    {
        if (0 < getId())
        {
            String authenticationToken = null;

            try
            {
                authenticationToken = confluenceWebTester.loginToXmlRPcService();
                final List<String> ids = (List<String>) confluenceWebTester.getXmlRpcClient()
                        .execute("functest-comment.getCommentIds", new Vector<>(asList(authenticationToken, String.valueOf(getId()))));
                return ids.stream()
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
            }
            catch (Exception e)
            {
                LOG.error(String.format("Error retrieving comments of content %d", getId()), e);
            }
            finally
            {
                confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
            }
        }

        return emptyList();
    }

    protected List<String> readLabels() throws RemoteException
    {
        if (0 != getId())
        {
            String soapSessionToken = null;
            ConfluenceSoapService confluenceSoapService;

            try
            {
                final RemoteLabel[] remoteLabels;
                final List<String> labels = new ArrayList<String>();

                soapSessionToken = confluenceWebTester.loginToSoapService();
                confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

                remoteLabels = confluenceSoapService.getLabelsById(soapSessionToken, getId());

                if (null != remoteLabels)
                {
                    final StringBuilder labelBuilder = new StringBuilder();

                    for (RemoteLabel remoteLabel : remoteLabels)
                    {
                        labelBuilder.setLength(0);

                        if (!"global".equals(remoteLabel.getNamespace()))
                        {
                            labelBuilder.append(remoteLabel.getNamespace()).append(':');
                        }

                        labelBuilder.append(remoteLabel.getName());
                        labels.add(labelBuilder.toString());
                    }
                }

                return labels;

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final ServiceException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromSoapService(soapSessionToken);
            }
        }

        return emptyList();
    }

    protected void updateLabels() throws RemoteException
    {
        if (0 != getId())
        {
            final List<String> remoteLabels = readLabels();

            String soapSessionToken = null;
            ConfluenceSoapService confluenceSoapService;

            try
            {
                soapSessionToken = confluenceWebTester.loginToSoapService();
                confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

                for (String label : getLabels())
                {
                    if (!remoteLabels.contains(label))
                    {
                        confluenceSoapService.addLabelByName(soapSessionToken, label, getId());
                    }
                }

                for (String remoteLabel : remoteLabels)
                {
                    if (!getLabels().contains(remoteLabel))
                    {
                        confluenceSoapService.removeLabelByName(soapSessionToken, remoteLabel, getId());
                    }
                }

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final ServiceException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromSoapService(soapSessionToken);
            }
        }
    }

    protected boolean saveOrUpdate()
    {
        try
        {
            if (super.saveOrUpdate())
            {
                updateLabels();
                return true;
            }
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }

        return false;
    }

    public boolean read()
    {
        if (super.read())
        {
            try
            {
                setLabels(readLabels());
                return true;
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
        }

        return false;
    }
}
