package com.atlassian.confluence.plugin.functest.module.xmlrpc.page;

import com.atlassian.confluence.rpc.SecureRpc;

import java.util.Map;

public interface PageHelperService extends SecureRpc
{
    Map<String, ?> getPage(final String authenticationToken, String pageId);
}
