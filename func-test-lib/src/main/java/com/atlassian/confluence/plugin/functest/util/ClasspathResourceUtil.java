package com.atlassian.confluence.plugin.functest.util;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;

public class ClasspathResourceUtil
{
    public static File getClassPathResourceAsTempFile(String classPathResource, ClassLoader classLoader, String fileSuffix)
            throws IOException
    {
        InputStream in = null;
        OutputStream out = null;

        try
        {
            in = classLoader.getResourceAsStream(classPathResource);

            if (null == in)
                return null; // Nothing to read for writing, returning temp file as is.

            File tempFile = File.createTempFile("com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil", fileSuffix);
            out = new BufferedOutputStream(new FileOutputStream(tempFile));

            IOUtils.copy(in, out);

            return tempFile;
        }
        finally
        {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }
}
