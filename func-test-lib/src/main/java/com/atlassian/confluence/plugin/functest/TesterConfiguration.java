package com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.util.EmbeddedMaven;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Properties;


public class TesterConfiguration
{
    private static final Logger LOG = Logger.getLogger(TesterConfiguration.class);

    private static final int DEFAULT_CONFLUENCE_WEBAPP_PORT = 1990;

    private String protocol;

    private String hostName;

    private int port;

    private String contextPath;

    private String adminUserName;

    private String adminPassword;

    private File pluginJar;

    private File testLibraryJar;

    private File siteBackupZip;

    private String license;

    private boolean installPlugin;


    public TesterConfiguration(Properties testProperties) throws IOException
    {
        initConfiguration(testProperties);
    }

    public TesterConfiguration() throws IOException
    {
        InputStream testPropertiesInput = getClass().getClassLoader().getResourceAsStream("webtester.properties");
        Properties testProperties = new Properties();

        if (null != testPropertiesInput)
        {
            try
            {
                testProperties.load(testPropertiesInput);
            }
            finally
            {
                IOUtils.closeQuietly(testPropertiesInput);
            }
        }
        else
        {
            LOG.debug("Cannot find webtester.properties in the class path. Defaults specified in https://developer.atlassian.com/jira/browse/CONFPLUGTEST-13 will be used.");
        }

        initConfiguration(testProperties);
    }

    private void initConfiguration(Properties testProperties)
            throws IOException
    {
        protocol = StringUtils.trim(StringUtils.defaultString(testProperties.getProperty("confluence.webapp.protocol"), "http"));
        hostName = StringUtils.trim(StringUtils.defaultString(testProperties.getProperty("confluence.webapp.host"), "localhost"));

        port = parsePort(
                StringUtils.trim(
                        StringUtils.defaultString(
                                System.getProperty("http.port"),
                                testProperties.getProperty("confluence.webapp.port")
                        )
                )
        );

        contextPath = parseContextPath(
                StringUtils.trim(
                        StringUtils.defaultString(
                            System.getProperty("context.path"),
                            StringUtils.defaultString(testProperties.getProperty("confluence.webapp.context.path"), "/confluence")
                        )
                )
        );

        adminUserName = StringUtils.trim(
                StringUtils.defaultString(
                        testProperties.getProperty("confluence.auth.admin.username"),
                        "admin"
                )
        );
        
        adminPassword = StringUtils.trim(
                StringUtils.defaultString(
                        testProperties.getProperty("confluence.auth.admin.password"),
                        "admin"
                )
        );

        String pluginJarPath = StringUtils.trim(testProperties.getProperty("confluence.plugin.jar"));

        if (StringUtils.isNotBlank(pluginJarPath))
            pluginJar = parsePluginJar(pluginJarPath);

        // Don't install the plugin if it is a bundled plugin.
        String bundledPluginProperty = StringUtils.trim(testProperties.getProperty("confluence.plugin.bundled"));
        // Can only be true if the property is defined and is "true". This property is not defined by AMPS.
        // Therefore, the test library won't help to install the plugin when AMPSified.
        installPlugin = null != bundledPluginProperty && !BooleanUtils.toBoolean(bundledPluginProperty);

        testLibraryJar = parseTestLibraryJar();

        String siteBackupZipPath = testProperties.getProperty("confluence.data.export");
        if (StringUtils.isNotBlank(siteBackupZipPath))
            siteBackupZip = parseSiteBackupZip(siteBackupZipPath);

        license = readLicense();
    }

    public String getHostName()
    {
        return hostName;
    }

    public int getPort()
    {
        return port;
    }

    public String getContextPath()
    {
        return contextPath;
    }

    public String getBaseUrl()
    {
        StringBuilder baseUrlBuilder = new StringBuilder(protocol).append("://").append(hostName);

        if (port != 80)
        {
            baseUrlBuilder.append(":").append(port);
        }

        return baseUrlBuilder.append(contextPath).toString();
    }

    public String getAdminUserName()
    {
        return adminUserName;
    }

    public String getAdminPassword()
    {
        return adminPassword;
    }

    public File getPluginJar()
    {
        return pluginJar;
    }

    public boolean isInstallPlugin()
    {
        return installPlugin;
    }

    public File getTestLibraryJar()
    {
        return testLibraryJar;
    }

    public File getSiteBackupZip()
    {
        return siteBackupZip;
    }

    public String getLicense()
    {
        return license;
    }

    private int parsePort(String portString)
    {
        try
        {
            int thePort = Integer.parseInt(portString);
            return 0 > thePort ? DEFAULT_CONFLUENCE_WEBAPP_PORT : thePort;
        }
        catch (NumberFormatException nfe)
        {
            return DEFAULT_CONFLUENCE_WEBAPP_PORT; // Default HTTP port reasonable?
        }
    }

    private String parseContextPath(String contextPath)
    {
        return contextPath.equals("/") ? "" : contextPath;
    }

    private File parsePluginJar(String pluginJarLocation)
    {

        File pluginJar = new File(pluginJarLocation);
        return pluginJar.isFile() ? pluginJar : null;
    }

    private File parseTestLibraryJar()
    {
        String libraryGav = BuildInfo.getGroupId() + ":" + BuildInfo.getArtifactId() + ":" + BuildInfo.getVersion();
        File testLibraryJar = EmbeddedMaven.resolveDep(libraryGav);

        boolean testLibraryJarExists = testLibraryJar != null && testLibraryJar.isFile();
        LOG.info("Checking if test library [" + libraryGav + "] can be found at: " + (testLibraryJar != null ? testLibraryJar.getAbsolutePath() : "{}") + ": " + testLibraryJarExists);

        return testLibraryJarExists ? testLibraryJar : null;
    }

    private File parseSiteBackupZip(String siteBackupZipLocation)
    {
        File siteBackupZip = new File(siteBackupZipLocation);
        return siteBackupZip.isFile() ? siteBackupZip : null;
    }

    private String readLicense() throws IOException
    {
        InputStream licenseInput = getClass().getClassLoader().getResourceAsStream("license.txt");
        if (null != licenseInput)
        {
            Reader licenseReader = new InputStreamReader(licenseInput, "UTF-8");
            Writer licenseWriter = new StringWriter();

            try
            {
                IOUtils.copy(licenseReader, licenseWriter);
                return licenseWriter.toString();
            }
            finally
            {
                IOUtils.closeQuietly(licenseWriter);
                IOUtils.closeQuietly(licenseReader);
            }
        }

        return null;
    }
}
