package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteComment;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

public class CommentHelper extends ContentHelper
{
    private static final Logger LOG = Logger.getLogger(CommentHelper.class);

    private long parentId;

    private long contentId;

    private String lastModifier;

    private Date lastModifiedDate;

    public CommentHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester);
        setId(id);
    }

    public CommentHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    public long getParentId()
    {
        return parentId;
    }

    public void setParentId(long parentId)
    {
        this.parentId = parentId;
    }

    public long getContentId()
    {
        return contentId;
    }

    public void setContentId(long contentId)
    {
        this.contentId = contentId;
    }

    public String getLastModifier()
    {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier)
    {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    protected RemoteComment toRemoteComment()
    {
        final RemoteComment remoteComment = new RemoteComment();

        remoteComment.setId(getId());
        remoteComment.setPageId(getContentId()); // This would need to be removed in the future, but as of Confluence 4.1-SNAPSHOT  Sep 6, 2011, it's still required
        remoteComment.setParentId(getParentId());
        remoteComment.setContent(getContent());
        remoteComment.setCreator(StringUtils.isBlank(getCreator()) ? confluenceWebTester.getCurrentUserName() : getCreator());

        if (null != getCreationDate())
        {
            final Calendar creationDate = Calendar.getInstance();

            creationDate.setTime(getCreationDate());
            remoteComment.setCreated(creationDate);
        }

        remoteComment.setModifier(StringUtils.isBlank(getLastModifier()) ? confluenceWebTester.getCurrentUserName() : getLastModifier());

        if (null != getLastModifiedDate())
        {
            final Calendar lastModifiedDate = Calendar.getInstance();

            lastModifiedDate.setTime(getLastModifiedDate());
            remoteComment.setModified(lastModifiedDate);
        }

        return remoteComment;
    }

    protected void populateHelper(Map<String, ?> commentStruct)
    {
        setId(Long.parseLong((String) commentStruct.get("id")));
        setParentId(
                commentStruct.containsKey("parentId")
                        ? Long.parseLong((String) commentStruct.get("parentId"))
                        : 0L
        );
        setContentId(Long.parseLong((String) commentStruct.get("ownerId")));
        setContent((String) commentStruct.get("content"));
        setCreator((String) commentStruct.get("creator"));
        setCreationDate((Date) commentStruct.get("created"));
        setLastModifier((String) commentStruct.get("lastModifier"));
        setLastModifiedDate((Date) commentStruct.get("lastModified"));
    }

    protected boolean storeRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        if (0 >= getContentId())
            throw new IllegalStateException("Comment persistence not allowed if contentId is lesser or equals to zero.");

        if (0 != getId())
            throw new IllegalStateException("At the time this method was written, we still do not have the ability to update comments via RPC. Therefore, I'm throwing this exception until we do.");

//        storedRemoteComment = confluenceSoapService.addComment(soapSessionToken, toRemoteComment());
//        setId(storedRemoteComment.getId());
//
//        return 0 < getId();

        boolean success = false;
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();

            Map<String, Object> commentStruct = new Hashtable<String, Object>();
            if (getId() > 0)
                commentStruct.put("id", String.valueOf(getId()));

            commentStruct.put("pageId", String.valueOf(getContentId()));
            if (getParentId() > 0)
                commentStruct.put("parentId", String.valueOf(getParentId()));
            commentStruct.put("content", StringUtils.defaultString(getContent()));


            String commentId = (String) confluenceWebTester.getXmlRpcClient().execute("functest-comment.addOrUpdateComment",
                    new Vector<Object>(
                            Arrays.<Object> asList(
                                    authenticationToken,
                                    commentStruct
                            )
                    ));

            if (success = StringUtils.isNotBlank(commentId))
                setId(Long.parseLong(commentId));
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to create/update a comment on page %d", getContentId()), e);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return success;
    }

    protected boolean readRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        boolean success = false;
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();

            @SuppressWarnings("unchecked")
            Map<String, ?> commentStruct = (Map<String, ?>) confluenceWebTester.getXmlRpcClient().execute("functest-comment.getComment",
                    new Vector<Object>(
                            Arrays.asList(
                                    authenticationToken,
                                    String.valueOf(getId())
                            )
                    ));
            if ((success = null != commentStruct))
                populateHelper(commentStruct);
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to find blog post with ID %d", getId()), e);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return success;
    }

    protected boolean deleteRemoteContent(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        return confluenceSoapService.removeComment(soapSessionToken, getId());
    }
}
