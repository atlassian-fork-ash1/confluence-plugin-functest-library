package com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.helper.UPMHelper;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapServiceServiceLocator;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.util.ConfluenceBuildUtil;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import com.thoughtworks.selenium.DefaultSelenium;
import junit.framework.Assert;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class SeleniumConfluenceWebTester extends DefaultSelenium implements ConfluenceWebTester
{
    private static final Logger LOG = Logger.getLogger(SeleniumConfluenceWebTester.class);

    private static final Set<String> KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY = new HashSet<String>();

    private static final String DEFAULT_PAGE_LOAD_TIMEOUT = "30000";

    private String currentUserName;

    private String currentPassword;

    private String licenseString;

    private final TesterConfiguration testerConfiguration;

    private HttpClientContext httpClientContext;

    public SeleniumConfluenceWebTester(TesterConfiguration testerConfiguration, int seleniumServerPort, String browserStartCommand)
    {
        super("localhost",
                seleniumServerPort,
                StringUtils.isBlank(browserStartCommand) ? "*firefox" : browserStartCommand,
                testerConfiguration.getBaseUrl());

        setCurrentUserName(testerConfiguration.getAdminUserName());
        setCurrentPassword(testerConfiguration.getAdminPassword());
        licenseString = testerConfiguration.getLicense();
        this.testerConfiguration = testerConfiguration;

        httpClientContext = UPMHelper.buildHttpClientContext(testerConfiguration);
    }

    protected String getDefaultPageLoadWaitTimeout()
    {
        return DEFAULT_PAGE_LOAD_TIMEOUT;
    }

    public void waitForPageToLoad()
    {
        waitForPageToLoad(getDefaultPageLoadWaitTimeout());
    }

    public String getContextPath()
    {
        return testerConfiguration.getContextPath();
    }

    public String getAdminUserName()
    {
        return testerConfiguration.getAdminUserName();
    }

    public String getAdminPassword()
    {
        return testerConfiguration.getAdminPassword();
    }

    public String getCurrentUserName()
    {
        return currentUserName;
    }

    public void setCurrentUserName(String currentUserName)
    {
        this.currentUserName = currentUserName;
    }

    public String getCurrentPassword()
    {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword)
    {
        this.currentPassword =currentPassword;
    }

    public String getLicenseString()
    {
        return licenseString;
    }

    public void setLicenseString(String licenseString)
    {
        this.licenseString = licenseString;
    }

    public String getBaseUrl()
    {
        return testerConfiguration.getBaseUrl();
    }

    private String getUrl(String pathRelativeToContext)
    {
        return new StringBuilder(getContextPath()).append(pathRelativeToContext).toString();
    }

    public void login(String username, String password)
    {
        open(getUrl("/login.action"));

        type("css=form[name='loginform'] input[name='os_username']", username);
        type("css=form[name='loginform'] input[name='os_password']", password);
        submit("css=form[name='loginform']");
        waitForPageToLoad();

        assertTrue(isElementPresent("css=a#logout-link"));
    }

    public void login()
    {
        login(getCurrentUserName(), getCurrentPassword());
    }

    public void logout()
    {
        click("css=a#logout-link");
        waitForPageToLoad();
    }

    public XmlRpcClient getXmlRpcClient() throws MalformedURLException
    {
        return new XmlRpcClient(new StringBuilder(getBaseUrl()).append("/rpc/xmlrpc").toString());
    }

    public String loginToXmlRpcService(String userName, String password) throws XmlRpcException, IOException
    {
        XmlRpcClient xmlRpcClient = getXmlRpcClient();

        return (String) xmlRpcClient.execute("confluence1.login",
                new Vector<String>(
                        Arrays.asList(userName, password)
                ));
    }

    public void logoutFromXmlRpcService(String authenticationToken)
    {
        XmlRpcClient xmlRpcClient;

        if (StringUtils.isNotBlank(authenticationToken))
        {
            try
            {
                xmlRpcClient = getXmlRpcClient();
                xmlRpcClient.execute("confluence1.logout",
                        new Vector<String>(
                                Arrays.asList(
                                        authenticationToken
                                )
                        ));
            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final XmlRpcException xmlRpcE)
            {
                LOG.error("Service request denied.", xmlRpcE);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
        }
    }

    public String loginToXmlRPcService() throws XmlRpcException, IOException
    {
        return loginToXmlRpcService(getCurrentUserName(), getCurrentPassword());
    }

    public ConfluenceSoapService getConfluenceSoapService() throws MalformedURLException, ServiceException
    {
        ConfluenceSoapServiceServiceLocator confluenceSoapServiceServiceLocator = new ConfluenceSoapServiceServiceLocator();
        return confluenceSoapServiceServiceLocator.getConfluenceserviceV1(
                new URL(new StringBuilder(getBaseUrl()).append("/rpc/soap-axis/confluenceservice-v1?wsdl").toString())
        );
    }

    public String loginToSoapService(String userName, String password) throws MalformedURLException, ServiceException, RemoteException
    {
        ConfluenceSoapService confluenceSoapService = getConfluenceSoapService();

        return StringUtils.isNotBlank(userName) ? confluenceSoapService.login(userName, password) : null;
    }

    public String loginToSoapService() throws MalformedURLException, ServiceException, RemoteException
    {
        return loginToSoapService(getCurrentUserName(), getCurrentPassword());
    }

    public void logoutFromSoapService(String authenticationToken)
    {
        try
        {
            if (null != authenticationToken)
            {
                getConfluenceSoapService().logout(authenticationToken);
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
    }

    public void updateLicense() throws IOException
    {
        // License is handled external to the application in Cloud
        // No reliable way to update in Cloud
        if (ConfluenceBuildUtil.isOnDemandMode()) {
            return;
        }

        String licenseString = getLicenseString();

        if (StringUtils.isNotBlank(licenseString))
        {
            try
            {
                assertTrue(gotoPageWithEscalatedPrivileges("/admin/license.action"));

                type("css=textarea[name='licenseString']", licenseString);
                click("css=input[name='update']");
                waitForPageToLoad();

                assertFalse(isTextPresent("License string is too short"));
                assertFalse(isTextPresent("License was not valid"));
            }
            finally
            {
                dropEscalatedPrivileges();
            }
        }
    }

    public void refreshLicense()
    {
        try
        {
            assertTrue(gotoPageWithEscalatedPrivileges("/admin/refreshlicensing.action"));
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    public void installTestLibrary()
    {
        File testLibraryJar = testerConfiguration.getTestLibraryJar();

        if (null != testLibraryJar && testLibraryJar.isFile())
        {
            try
            {
                assertTrue(gotoPageWithEscalatedPrivileges("/admin/console.action"));

                String pluginKey = PluginMetadataUtil.getPluginKey(
                        PluginMetadataUtil.getPluginDescriptorDom(testLibraryJar)
                );
                pluginKey = UPMHelper.buildUPMPluginOrModuleKey(pluginKey);
                if (!isPluginInstalled(pluginKey))
                {
                    LOG.info("Installing test library with key " + pluginKey);
                    installPlugin(testLibraryJar);
                }
                else
                {
                    LOG.info("Plugin test library already installed. Skipping installation.");
                }

            }
            catch (Exception e)
            {
                fail("Unable to determine plugin key from " + testLibraryJar + SystemUtils.LINE_SEPARATOR +
                        ExceptionUtils.getFullStackTrace(e));
            }
            finally
            {
                dropEscalatedPrivileges();
            }
        }
    }

    @Override
    public boolean isPluginInstalled(String pluginKey)
    {
        return UPMHelper.isPluginInstalled(pluginKey, testerConfiguration, httpClientContext);
    }

    @Override
    public boolean isPluginEnabled(String pluginKey)
    {
        return UPMHelper.isPluginEnabled(pluginKey, testerConfiguration, httpClientContext);
    }

    @Override
    public boolean enablePlugin(String pluginKey, boolean enabled)
    {
        return UPMHelper.enablePlugin(pluginKey, enabled, testerConfiguration, httpClientContext);
    }

    @Override
    public boolean isPluginModuleEnabled(String pluginKey, String moduleKey)
    {
        return UPMHelper.isPluginModuleEnabled(pluginKey, moduleKey, testerConfiguration, httpClientContext);
    }

    @Override
    public boolean enablePluginModule(String pluginKey, String moduleKey, boolean enabled)
    {
        return UPMHelper.enablePluginModule(pluginKey, moduleKey, enabled, testerConfiguration, httpClientContext);
    }

    public void installPlugin()
    {
        File pluginJar = testerConfiguration.getPluginJar();
        if (testerConfiguration.isInstallPlugin() && null != pluginJar)
            installPlugin(pluginJar);
    }

    public void installPlugin(File pluginJar)
    {
        if (null != pluginJar && pluginJar.isFile())
        {
            try
            {
                Document pluginDescDom = PluginMetadataUtil.getPluginDescriptorDom(pluginJar);
                String pluginKey = PluginMetadataUtil.getPluginKey(pluginDescDom);
                String pluginName = PluginMetadataUtil.getPluginName(pluginDescDom);
                pluginKey = UPMHelper.buildUPMPluginOrModuleKey(pluginKey);
                if (!hasTestLibraryInstalledPlugin(pluginKey))
                {
                    LOG.info("Installing plugin \"" + pluginName + "\" with key " + pluginKey);
                    installPluginInternal(pluginJar);
                    markPluginInstalledByTestLibrary(pluginKey);
                }
                else
                {
                    LOG.info("Plugin \"" + pluginName + "\" already installed. Skipping installation.");
                }
            }
            catch (Exception e)
            {
                fail("Unable to determine plugin key from " + pluginJar + SystemUtils.LINE_SEPARATOR + ExceptionUtils.getFullStackTrace(e));
            }
        }
        else
        {
            LOG.warn("Invalid plugin JAR (" + pluginJar + ") specified for install. Skipping");
        }
    }

    private boolean hasTestLibraryInstalledPlugin(String pluginKey)
    {
        return KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY.contains(pluginKey);
    }

    private void markPluginInstalledByTestLibrary(String pluginKey)
    {
        KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY.add(pluginKey);
    }

    protected Document parseToDocument(InputStream responseIn)
    {
        PrintWriter errorWriter = new PrintWriter(new StringWriter());

        Tidy tidy = new Tidy();
        tidy.setErrout(errorWriter);
        tidy.setInputEncoding("UTF-8");

        return tidy.parseDOM(responseIn, null);
    }

    protected XPathExpression compileXPathExpression(String xPathExpression) throws XPathExpressionException
    {
        return XPathFactory.newInstance().newXPath().compile(xPathExpression);
    }

    private String getXsrfToken(InputStream responseBody) throws IOException
    {
        try
        {
            return compileXPathExpression("//head/meta[@id='atlassian-token']/@content").evaluate(parseToDocument(responseBody));
        }
        catch (XPathExpressionException xpee)
        {
            LOG.error("Compile time XPath expression not valid?", xpee);
            fail("Unable to get atlassian-token");
        }

        return null;
    }

    private String loginToLocationWithEscalatedPrivileges(HttpClient httpClient, String pathRelativeToBaseUrl) throws IOException
    {
        if (ConfluenceBuildUtil.containsSudoFeature())
        {
            GetMethod loginMethod = null;

            try
            {
                (loginMethod = new GetMethod(new StringBuilder(getBaseUrl()).append("/authenticate.action").toString())).setQueryString(
                        new NameValuePair[]
                                {
                                    new NameValuePair("destination", pathRelativeToBaseUrl),
                                    new NameValuePair("os_username", getCurrentUserName()),
                                    new NameValuePair("os_password", getCurrentPassword())
                                }
                );

                assertEquals(HttpServletResponse.SC_OK, httpClient.executeMethod(loginMethod));


                GetMethod getEscalatedPrivsMethod = null;
                InputStream destinationBodyInput = null;
                try
                {
                    (getEscalatedPrivsMethod = new GetMethod(new StringBuilder(getBaseUrl()).append("/doauthenticate.action").toString())).setQueryString(
                            new NameValuePair[]
                                    {
                                        new NameValuePair("destination", pathRelativeToBaseUrl),
                                        new NameValuePair("password", getCurrentPassword())
                                    }
                    );

                    httpClient.executeMethod(getEscalatedPrivsMethod);

                    destinationBodyInput = getEscalatedPrivsMethod.getResponseBodyAsStream();
                    return getXsrfToken(destinationBodyInput);
                }
                finally
                {
                    IOUtils.closeQuietly(destinationBodyInput);
                    if (null != getEscalatedPrivsMethod)
                        getEscalatedPrivsMethod.releaseConnection();
                }
            }
            finally
            {
                if (null != loginMethod)
                    loginMethod.releaseConnection();
            }
        }
        else
        {
            GetMethod loginMethod = null;
            InputStream destinationBodyInput = null;

            try
            {
                (loginMethod = new GetMethod(new StringBuilder(getBaseUrl()).append(pathRelativeToBaseUrl).toString())).setQueryString(
                        new NameValuePair[]
                                {
                                    new NameValuePair("os_username", getCurrentUserName()),
                                    new NameValuePair("os_password", getCurrentPassword())
                                }
                );

                httpClient.executeMethod(loginMethod);

                destinationBodyInput = loginMethod.getResponseBodyAsStream();
                return getXsrfToken(destinationBodyInput);
            }
            finally
            {
                IOUtils.closeQuietly(destinationBodyInput);
                if (null != loginMethod)
                    loginMethod.releaseConnection();
            }
        }
    }

    private void logoutWithHttpClient(HttpClient httpClient)
    {
        GetMethod logoutMethod = new GetMethod(new StringBuilder(getBaseUrl()).append("/logout.action").toString());

        try
        {
            httpClient.executeMethod(logoutMethod);
        }
        catch (IOException ioe)
        {
            LOG.error("Unable to logout with HttpClient.", ioe);
        }
        finally
        {
            logoutMethod.releaseConnection();
        }
    }

    public void uploadFile(HttpClient httpClient, String path, Map<String, String[]> params, String fileParamName, File file, HttpClientCallback<PostMethod> httpClientCallback) throws IOException
    {
        PostMethod postMethod = new PostMethod(new StringBuilder(getBaseUrl()).append(path).toString());

        Collection<Part> parts = new ArrayList<Part>();

        if (null != params)
            for (Map.Entry<String, String[]> e : params.entrySet())
            {
                String name = e.getKey();
                String[] values = e.getValue();

                if (StringUtils.isNotBlank(name) && null != values && values.length > 0)
                    for (String value : values)
                        parts.add(new StringPart(name, value));
            }

        parts.add(new FilePart(fileParamName, file));

        postMethod.setRequestEntity(new MultipartRequestEntity(
                parts.toArray(new Part[parts.size()]),
                httpClient.getParams()
        ));

        try
        {
            int statusCode = httpClient.executeMethod(postMethod);
            if (null != httpClientCallback)
                httpClientCallback.handle(httpClient, statusCode, postMethod);
        }
        finally
        {
            postMethod.releaseConnection();
            logoutWithHttpClient(httpClient);
        }
    }

    public void installPluginInternal(File pluginJar)
    {
        UPMHelper.installPlugin(pluginJar, testerConfiguration, httpClientContext);
    }

    /**
     * Restore the class path resource &quot;site-export.zip&quot; into Confluence.
     * @deprecated
     * Please use {@link #restoreData(java.io.File)} instead.
     */
    public void restoreData()
    {
        File configuredSiteExport = testerConfiguration.getSiteBackupZip();
        File siteExportZip  = null;

        try
        {
            if (null != configuredSiteExport)
            {
                // Restore the configured site backup
                restoreData(configuredSiteExport);
            }
            else
            {
                // Try to restore a file named site-export.zip for backwards compatibility.
                // The 'legacy' parent POM sets ${basedir}src/test/resources/site-export.zip as the configured site backup to restore.
                siteExportZip = ClasspathResourceUtil.getClassPathResourceAsTempFile("site-export.zip", getClass().getClassLoader(), ".zip");
                if (null != siteExportZip)
                    restoreData(siteExportZip);
            }
        }
        catch (IOException ioe)
        {
            Assert.fail("Unable to restore class path site-export.zip: " + ioe.getMessage());
        }
        finally
        {
            if (null != siteExportZip && siteExportZip.exists())
                Assert.assertTrue("Unable to delete " + siteExportZip, siteExportZip.delete());
        }
    }

    public void restoreData(File exportZip)
    {
        HttpClient httpClient = new HttpClient();

        if (null != exportZip && exportZip.isFile())
        {
            try
            {

                Map<String, String[]> params = new HashMap<String, String[]>();

                params.put("synchronous", new String[] { "true" });
                params.put("atl_token", new String[] { loginToLocationWithEscalatedPrivileges(httpClient, "/admin/restore.action") });

                uploadFile(httpClient, "/admin/restore.action", params, "file_0", exportZip, new HttpClientCallback<PostMethod>()
                {
                    public void handle(HttpClient httpClient, int statusCode, PostMethod uploadMethod)
                    {
                        assertEquals(HttpServletResponse.SC_MOVED_TEMPORARILY, statusCode);

                        open(getUrl("/admin/editgeneralconfig.action"));
                        type("css=form[name='editgeneralconfig'] input[name='domainName']", getBaseUrl());
                        submit("css=form[name='editgeneralconfig']");
                        waitForPageToLoad();
                        HelperFactory.createIndexHelper(SeleniumConfluenceWebTester.this).update();
                    }
                });
            }
            catch (IOException ioe)
            {
                LOG.error("Unable to restore " + exportZip, ioe);
                fail("Unable to restore " + exportZip);
            }
            finally
            {
                logoutWithHttpClient(httpClient);
            }
        }
        else
        {
            LOG.warn("Unable to find export file: " + exportZip + ". Skipping data restoration.");
        }
    }

    public void flushCaches()
    {
        try
        {
            assertTrue(gotoPageWithEscalatedPrivileges("/admin/cachestatistics.action"));

            String atlassianToken = null;
            String atlassianTokenXpath = "//meta[@id='atlassian-token']";
            if (isElementPresent(atlassianTokenXpath))
                atlassianToken = getAttribute("xpath=" + atlassianTokenXpath + "/@content");

            gotoPageWithEscalatedPrivileges("/admin/flushcache.action" + (StringUtils.isBlank(atlassianToken) ? "" : "?atl_token=" + atlassianToken));
            waitForPageToLoad();
        }
        finally
        {
            dropEscalatedPrivileges();
        }

    }

    private String assertXsrfStatusCode(String resourcePath)
    {
        StringBuilder urlWithOsAuthTypeReplacedBuilder = new StringBuilder(getBaseUrl())
                .append(
                        resourcePath.replaceAll(
                                "(.*[?&])(os_authType=\\w*)(.*)",
                                "$1os_authType=basic$3"
                        )
                );

        if (urlWithOsAuthTypeReplacedBuilder.indexOf("os_authType") < 0)
        {
            urlWithOsAuthTypeReplacedBuilder.append(urlWithOsAuthTypeReplacedBuilder.indexOf("?") < 0 ? '?' : '&')
                    .append("os_authType=basic");
        }

        HttpClient client = new HttpClient();
        GetMethod httpGet = new GetMethod(urlWithOsAuthTypeReplacedBuilder.toString());

        try
        {
            client.getState().setCredentials(
                    new AuthScope(
                            AuthScope.ANY_HOST,
                            AuthScope.ANY_PORT
                    ),
                    new UsernamePasswordCredentials(
                            getCurrentUserName(),
                            getCurrentPassword()
                    )
            );
            httpGet.setDoAuthentication(true);

            assertEquals(
                    HttpServletResponse.SC_FORBIDDEN,
                    client.executeMethod(httpGet)
            );

            return httpGet.getResponseBodyAsString();
        }
        catch (IOException ioe)
        {
            LOG.error("Error getting HTTP resource " + urlWithOsAuthTypeReplacedBuilder, ioe);
            return null;
        }
        finally
        {
            httpGet.releaseConnection();
        }
    }

    /**
     * Asserts that a resource is XSRF protected.
     *
     * @param resourcePath The resource to test for XSRF protection. It must not contain the XSRF token. Must be absolute (e.g. <tt>/admin/plugins.action</tt>).
     */
    public void assertResourceXsrfProtected(String resourcePath)
    {
        assertFalse("No body text returned for " + resourcePath, StringUtils.isBlank(assertXsrfStatusCode(resourcePath)));
    }

    public void assertXsrfTokenNotPresentFailure(String resourcePath)
    {
        assertTrue(
                assertXsrfStatusCode(resourcePath).indexOf("a required security token was not present") >= 0
        );
    }

    public void assertXsrfTokenNotValidFailure(String resourcePath)
    {
        assertTrue(
                assertXsrfStatusCode(resourcePath).indexOf("Your session has expired. You may need to re-submit the form or reload the page.") >= 0
        );
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination)
    {
        return gotoPageWithEscalatedPrivileges(destination, "UTF-8");
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination, String urlEncoding)
    {
        if (ConfluenceBuildUtil.containsSudoFeature() && !isElementPresent("css=#confluence-message-websudo-message"))
        {
            try
            {
                open(getUrl("/authenticate.action?destination=" + URLEncoder.encode(destination, urlEncoding)));
                waitForPageToLoad();

                type("xpath=//input[@name='password']", getCurrentPassword());
                click("xpath=//input[@name='authenticate']");
                waitForPageToLoad();
            }
            catch (UnsupportedEncodingException uee)
            {
                LOG.error("Unable to URL encode " + destination + " with " + urlEncoding, uee);
                return false;
            }
        }
        else
        {
            open(getUrl(destination));
            waitForPageToLoad();
        }

        return true;
    }

    public void dropEscalatedPrivileges()
    {
        if (ConfluenceBuildUtil.containsSudoFeature() && isElementPresent("css=#confluence-message-websudo-message]"))
        {
            open(getUrl("/dropauthentication.action"));
            waitForPageToLoad();
        }
    }

    @Override
    public void uninstallPlugin(String pluginKey)
    {
        UPMHelper.uninstallPlugin(pluginKey, testerConfiguration, httpClientContext);
    }

    public static interface HttpClientCallback<T extends HttpMethodBase>
    {
        void handle(HttpClient httpClient, int statusCode, T method);
    }
}
