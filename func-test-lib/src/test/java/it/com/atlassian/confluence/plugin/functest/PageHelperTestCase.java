package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PageHelperTestCase extends AbstractConfluencePluginWebTestCase
{

    private boolean createPage(final PageHelper pageHelper, final String spaceKey, final String title, final String content)
    {
        pageHelper.setTitle(title);
        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setContent(content);

        return pageHelper.create();
    }

    public void testCreatePage()
    {
        final PageHelper pageHelper = getPageHelper();

        assertTrue(createPage(pageHelper, "ds", "Test Create Page", "This is a test page"));
        assertFalse(0 == pageHelper.getId());

        gotoPage("/display/" + pageHelper.getSpaceKey() + "/Test+Create+Page");
        assertTextPresent("This is a test page");

        assertTrue(pageHelper.delete());
    }

    public void testEditPage()
    {
        final PageHelper pageHelper = getPageHelper();
        final long createdPageId;

        assertTrue(createPage(pageHelper, "ds", "Test Edit Page", "This is a test page to be edited."));
        assertFalse(0 == (createdPageId = pageHelper.getId()));

        gotoPage("/display/" + pageHelper.getSpaceKey() + "/Test+Edit+Page");
        assertTextPresent("This is a test page to be edited.");

        pageHelper.setTitle("Test Edited Page");
        pageHelper.setContent("This is an edited test page.");

        assertTrue(pageHelper.update());

        /* Ensure we did not create a new page */
        assertEquals(createdPageId, pageHelper.getId());

        gotoPage("/display/" + pageHelper.getSpaceKey() + "/Test+Edited+Page");
        assertTextPresent("This is an edited test page.");

        assertTrue(pageHelper.delete());
    }

    public void testDeletePage()
    {
        final PageHelper pageHelper = getPageHelper();

        assertTrue(createPage(pageHelper, "ds", "Test Delete Page", "This is a page to be removed."));
        assertFalse(0 == pageHelper.getId());

        gotoPage("/display/" + pageHelper.getSpaceKey() + "/Test+Delete+Page");
        assertTextPresent("This is a page to be removed.");

        assertTrue(pageHelper.delete());
    }

    public void testReadPage()
    {
        final PageHelper pageHelper = getPageHelper();
        final long creatingPagesAndLinkingPageId;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Creating pages and linking");

        creatingPagesAndLinkingPageId = pageHelper.findBySpaceKeyAndTitle();
        assertTrue(creatingPagesAndLinkingPageId > 0);

        pageHelper.setId(creatingPagesAndLinkingPageId);
        assertTrue(pageHelper.read());

        assertEquals("ds", pageHelper.getSpaceKey());
        assertTrue(pageHelper.getParentId() > 0); /* The page return should be a child page of DS:Confluence Overview */
        assertEquals("Creating pages and linking", pageHelper.getTitle());
        assertTrue(pageHelper.getVersion() > 0);

        assertTrue(pageHelper.getContent().indexOf("Creating new pages through links") >= 0);
    }

    public void testGetChildrenPages()
    {
        final PageHelper pageHelper = getPageHelper();
        final long creationPagesAndLinkingPageId;
        final List childrenPageIds;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Creating pages and linking");

        assertTrue(0 < (creationPagesAndLinkingPageId = pageHelper.findBySpaceKeyAndTitle()));

        pageHelper.setId(creationPagesAndLinkingPageId);

        childrenPageIds = pageHelper.getChildrenPageIds();
        assertEquals(1, childrenPageIds.size());

        pageHelper.setId(((Number) childrenPageIds.get(0)).longValue());
        assertTrue(pageHelper.read()); /* Read the contents of the children page */

        assertEquals(((Number) childrenPageIds.get(0)).longValue(), pageHelper.getId());
        assertEquals(creationPagesAndLinkingPageId, pageHelper.getParentId());
        assertEquals("ds", pageHelper.getSpaceKey());
        assertEquals("Breadcrumb demonstration", pageHelper.getTitle());
    }

    public void testGetCommentOnPage()
    {
        final PageHelper pageHelper = getPageHelper();
        final List commentIds;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        CommentHelper commentHelper = getCommentHelper();
        String commentContent = "testCommentContent";
        commentHelper.setContent(commentContent);

        long contentId = pageHelper.findBySpaceKeyAndTitle();
        commentHelper.setContentId(contentId);

        assertTrue(commentHelper.create());

        pageHelper.setId(contentId);

        commentIds = pageHelper.getCommentIds();
        assertFalse(commentIds.isEmpty());
        assertTrue(0 < ((Number) commentIds.get(0)).longValue());

        gotoPage("/display/ds/Confluence+Overview?showComments=true&focusedCommentId=" + commentIds.get(0));
        assertTextPresent(commentContent);
    }

    public void testAddLabelToChildPage()
    {
        final PageHelper pageHelper = getPageHelper();
        final long creatingPagesAndLinkingPageId;
        final List labels;
        final PageHelper anotherPageHelper;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Creating pages and linking");

        creatingPagesAndLinkingPageId = pageHelper.findBySpaceKeyAndTitle();
        assertTrue(creatingPagesAndLinkingPageId > 0);

        pageHelper.setId(creatingPagesAndLinkingPageId);
        assertTrue(pageHelper.read());

        pageHelper.setLabels(Arrays.asList("my:label1", "team:label2", "label3"));
        assertTrue(pageHelper.update());

        anotherPageHelper = getPageHelper(pageHelper.getId());
        anotherPageHelper.read();

        labels = anotherPageHelper.getLabels();

        assertTrue(labels.containsAll(Arrays.asList("my:label1", "team:label2", "label3")));
    }

    public void testReduceLabelsOnPage()
    {
        final PageHelper pageHelper = getPageHelper();
        final long creatingPagesAndLinkingPageId;
        final List labels;
        final PageHelper anotherPageHelper;
        final PageHelper yetAnotherPageHelper;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Creating pages and linking");

        creatingPagesAndLinkingPageId = pageHelper.findBySpaceKeyAndTitle();
        assertTrue(creatingPagesAndLinkingPageId > 0);

        pageHelper.setId(creatingPagesAndLinkingPageId);
        assertTrue(pageHelper.read());

        pageHelper.setLabels(Arrays.asList("my:label1", "team:label2"));
        assertTrue(pageHelper.update());

        anotherPageHelper = getPageHelper(pageHelper.getId());
        anotherPageHelper.read();

        labels = anotherPageHelper.getLabels();
        assertTrue(labels.containsAll(Arrays.asList("my:label1", "team:label2")));


        anotherPageHelper.setLabels(Arrays.asList("my:label1"));
        assertTrue(anotherPageHelper.update());

        yetAnotherPageHelper = getPageHelper(pageHelper.getId());
        assertTrue(yetAnotherPageHelper.read());

        assertEquals(1, yetAnotherPageHelper.getLabels().size());
        assertTrue(yetAnotherPageHelper.getLabels().containsAll(Arrays.asList("my:label1")));
    }

    public void testAddWatch()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Page To Watch");
        pageHelper.setContent("Foobar");

        assertTrue(pageHelper.create());
        pageHelper.removeWatch();
        assertFalse(pageHelper.isWatched());

        pageHelper.addWatch();

        assertTrue(pageHelper.isWatched());
    }

    public void testMarkAsFavorite()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Favorite Page");
        pageHelper.setContent("Foobar");

        assertTrue(pageHelper.create());
        assertFalse(pageHelper.isFavorite());

        pageHelper.markFavourite();

        assertTrue(pageHelper.isFavorite());
        assertTrue(pageHelper.delete());
    }

    public void testGetAttachmentNames() throws IOException
    {
        final String[] attachmentNames = new String[]{"file1.txt", "file2.txt"};
        final PageHelper pageHelper = getPageHelper();
        final List actualAttachmentNames;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Test page which contains attachments.");
        pageHelper.setContent("Foobar");

        assertTrue(pageHelper.create());

        for (String attachmentName : attachmentNames)
        {
            final AttachmentHelper attachmentHelper = getAttachmentHelper();
            final byte[] data = attachmentName.getBytes("UTF-8");

            attachmentHelper.setParentId(pageHelper.getId());
            attachmentHelper.setFileName(attachmentName);
            attachmentHelper.setContentType("text/plain");
            attachmentHelper.setContentLength(data.length);
            attachmentHelper.setContent(data);

            assertTrue(attachmentHelper.create());
        }

        actualAttachmentNames = Arrays.asList(pageHelper.getAttachmentFileNames());

        assertEquals(2, actualAttachmentNames.size());
        assertTrue(Arrays.asList(attachmentNames).containsAll(actualAttachmentNames));
        assertTrue(pageHelper.delete());
    }

    public void testConvertMarkupToXhtml()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("testConvertMarkupToXhtml");
        pageHelper.setContent("");

        assertTrue(pageHelper.create());

        assertEquals("Line 1\n" +
                "Line 2", pageHelper.getXhtmlAsMarkup("<div>Line 1<br />Line 2</div>"));
    }

    public void testMarkupToXhtml()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("testMarkupToXhtml");
        pageHelper.setContent("*foo*");

        assertTrue(pageHelper.create());

        assertEquals("<p><strong>foo</strong></p>", pageHelper.getContentAsXhtml());
    }
}
