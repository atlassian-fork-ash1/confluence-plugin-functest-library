package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.apache.commons.io.IOUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

public class UserHelperTestCase extends AbstractConfluencePluginWebTestCase
{
    private File backupFile;

    private File copyDemoSiteToBackupFile() throws IOException
    {
        InputStream in = null;
        OutputStream out = null;

        try
        {
            File backupFile = File.createTempFile("it.com.atlassian.confluence.plugin.functest", ".zip");
            in = getClass().getClassLoader().getResourceAsStream("confluence-demo-site.zip");
            out = new BufferedOutputStream(new FileOutputStream(backupFile));

            IOUtils.copy(in, out);

            return backupFile;
        }
        finally
        {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        backupFile = copyDemoSiteToBackupFile();
        getConfluenceWebTester().restoreData(backupFile);
        restoreData();
    }

    protected void tearDown() throws Exception
    {
        try
        {
            assertTrue(backupFile.delete());
        }
        finally
        {
            super.tearDown();
        }
    }

    protected void createUser(UserHelper userHelper)
    {
        userHelper.setName("john.doe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress("john.doe@localhost.localdomain");
        userHelper.setPassword("foobar");
        userHelper.setGroups(Arrays.asList("confluence-users"));

        assertTrue(userHelper.create());
    }

    public void testCreateUser()
    {
        final UserHelper userHelper = getUserHelper();

        createUser(userHelper);

        logout();
        login(userHelper.getName(), "foobar");

        assertUserExistsOnPage(userHelper);

        logout();
        loginAsAdmin();

        assertTrue(userHelper.delete());
    }

    private void assertUserExistsOnPage(final UserHelper userHelper)
    {
        assertElementPresentByXPath("//meta[@content='" + userHelper.getName() + "' and @name='ajs-remote-user']");
        assertElementPresentByXPath("//meta[@content='" + userHelper.getFullName() + "' and @name='ajs-current-user-fullname']");
    }

    public void testEditUser()
    {
        final UserHelper userHelper = getUserHelper();

        createUser(userHelper);

        userHelper.setFullName("John Smith Doe");
        userHelper.setEmailAddress("john.smith.doe@localhost.localdomain");
        assertTrue(userHelper.update());

        logout();
        login(userHelper.getName(), "foobar");

        gotoPage("/users/viewuserprofile.action?username=" + userHelper.getName());

        assertTextPresent(userHelper.getFullName());
        assertTextPresent(userHelper.getEmailAddress());

        logout();
        loginAsAdmin();

        assertTrue(userHelper.delete());
    }

    public void testChangePassword()
    {
        final UserHelper userHelper = getUserHelper();

        createUser(userHelper);

        userHelper.setPassword("barfoo");
        assertTrue(userHelper.updatePassword());

        logout();
        login(userHelper.getName(), "barfoo");

        assertUserExistsOnPage(userHelper);

        logout();
        loginAsAdmin();

        assertTrue(userHelper.delete());
    }

    public void testAddAndRemoveGroupMembership()
    {
        final UserHelper userHelper = getUserHelper();

        createUser(userHelper);

        try
        {
            /* Ensure user is a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-users");
            assertLinkPresentWithText(userHelper.getName());
            /* Ensure user is not a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-administrators");
            assertLinkNotPresentWithText(userHelper.getName());

            /** Add membership */
            userHelper.setGroups(Arrays.asList("confluence-users", "confluence-administrators"));
            assertTrue(userHelper.update());

            /* Ensure user is a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-users");
            assertLinkPresentWithText(userHelper.getName());
            /* Ensure user is a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-administrators");
            assertLinkPresentWithText(userHelper.getName());

            /* Remove confluence-administrators membership */
            userHelper.setGroups(Arrays.asList("confluence-users"));
            assertTrue(userHelper.update());

            /* Ensure user is a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-users");
            assertLinkPresentWithText(userHelper.getName());
            /* Ensure user is not a member of confluence-administrators */
            gotoPageWithEscalatedPrivileges("/admin/users/domembersofgroupsearch.action?membersOfGroupTerm=confluence-administrators");
            assertLinkNotPresentWithText(userHelper.getName());

            assertTrue(userHelper.delete());
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    public void testRead()
    {
        final UserHelper userHelper = getUserHelper();
        final UserHelper anotherUserHelper;

        createUser(userHelper);

        anotherUserHelper = getUserHelper(userHelper.getName());
        assertTrue(anotherUserHelper.read());

        assertEquals(userHelper.getName(), anotherUserHelper.getName());
        assertEquals(userHelper.getFullName(), anotherUserHelper.getFullName());
        assertEquals(userHelper.getEmailAddress(), anotherUserHelper.getEmailAddress());
        assertEquals(userHelper.getGroups(), anotherUserHelper.getGroups());

        assertTrue(anotherUserHelper.delete());
    }
}
