package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.UPMHelper;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import junit.framework.Assert;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class InstallPluginTestCase extends AbstractConfluencePluginWebTestCase
{

    private File pluginFile;

    protected void setUp() throws Exception
    {
        super.setUp();
        pluginFile = ClasspathResourceUtil.getClassPathResourceAsTempFile("themes-topandleftnavigation.jar", getClass().getClassLoader(), ".jar");
    }

    protected void tearDown() throws Exception
    {
        if (null != pluginFile)
            assertTrue(pluginFile.delete());

        super.tearDown();
    }

    // Need a dummy test in this class otherwise the runner complains
    public void testIgnored() {
        assertTrue(true);
    }

    // Ignored - cannot install plugins in Cloud.
    public void _testInstallPlugin()
    {
        String pluginKey = "";
        final String MODULE_KEY = "themeaction-key";
        ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        confluenceWebTester.installPlugin(pluginFile);
        try
        {
            pluginKey = PluginMetadataUtil.getPluginKey(PluginMetadataUtil.getPluginDescriptorDom(pluginFile));
            pluginKey = UPMHelper.buildUPMPluginOrModuleKey(pluginKey);
            assertThat(confluenceWebTester.isPluginInstalled(pluginKey), is(true));
            assertThat(confluenceWebTester.isPluginEnabled(pluginKey), is(true));
            assertThat(confluenceWebTester.isPluginModuleEnabled(pluginKey, MODULE_KEY), is(true));

            confluenceWebTester.enablePlugin(pluginKey, false);
            assertThat(confluenceWebTester.isPluginEnabled(pluginKey), is(false));

            confluenceWebTester.enablePlugin(pluginKey, true);
            assertThat(confluenceWebTester.isPluginEnabled(pluginKey), is(true));

            confluenceWebTester.enablePluginModule(pluginKey, MODULE_KEY, false);
            assertThat(confluenceWebTester.isPluginModuleEnabled(pluginKey, MODULE_KEY), is(false));

            confluenceWebTester.enablePluginModule(pluginKey, MODULE_KEY, true);
            assertThat(confluenceWebTester.isPluginModuleEnabled(pluginKey, MODULE_KEY), is(true));
        }
        catch (Exception e)
        {
            fail("Unable to get plugin name from plugin JAR at " + pluginFile);
        }
        finally
        {
            confluenceWebTester.uninstallPlugin(pluginKey);
        }
    }

}
