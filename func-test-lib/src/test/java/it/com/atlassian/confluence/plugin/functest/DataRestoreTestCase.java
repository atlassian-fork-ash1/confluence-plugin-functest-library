package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;

import java.io.File;

public class DataRestoreTestCase extends AbstractConfluencePluginWebTestCase
{
    private File backupFile;

    protected void setUp() throws Exception
    {
        super.setUp();
        backupFile = ClasspathResourceUtil.getClassPathResourceAsTempFile("confluence-demo-site.zip", getClass().getClassLoader(), ".zip");
        this.getTestingEngine().setIgnoreFailingStatusCodes(true);
    }

    protected void tearDown() throws Exception
    {
        if (null != backupFile)
            assertTrue(backupFile.delete());
        this.getTestingEngine().setIgnoreFailingStatusCodes(false);
        super.tearDown();
    }

    public void testRestoreData()
    {
        final String PAGE_PATH = "/display/ds";
        final String LINK_TEXT = "Confluence Overview";

        // dashboard.action has been changed in 5.9.1 so we cannot use it to test with both 5.9.1 and earlier.
        gotoPage(PAGE_PATH);
        // Because of rendered html code is not 100% correct so JWebUnit cannot get correct information. Therefore, we cannot check "Demonstration Space" link in side-bar.
        // The issue should come from side-bar plugin.
        assertLinkPresentWithText(LINK_TEXT);

        assertTrue(getSpaceHelper("ds").delete());

        gotoPage(PAGE_PATH);
        assertTextPresent("Page Not Found");

        getConfluenceWebTester().restoreData(backupFile);

        gotoPage(PAGE_PATH);
        assertLinkPresentWithText(LINK_TEXT);
    }
}
